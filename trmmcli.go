// SPDX-License-Identifier: PostgreSQL

// Package trmmcli is the higher level interface to Tactical RMM.
package trmmcli

import (
	"bytes"
	"fmt"
	"runtime"
	"text/template"

	"github.com/rs/zerolog/log"
	"gitlab.com/NiceGuyIT/trmm-cli/trmm"
)

// TacticalCLI is the main entry to the TRMM CLI.
type TacticalCLI struct {
	cli    *CLI
	trmm   *trmm.TRMM
	config *TacticalConfig
	output string
	bytes  []byte
	err    error
}

// TacticalConfig is the configuration for Tactical RMM.
type TacticalConfig struct {
	domain  string
	apiKey  string
	timeout int `default:"0"`
}

// External variables pulled in during the build process
var (
	// VERSION TODO: Add API version, GUI version, and possibly other versions.
	VERSION    = "unknown version"
	BUILD_TIME = "unknown time"
	BUILD_ENV  = "unknown env"
	GIT_COMMIT = "unknown git commit"
	GIT_TAG    = "unknown git tag"
	GIT_BRANCH = "unknown git branch"
)

const (
	NAME         = "trmm-cli"
	URL          = "https://gitlab.com/NiceGuyIT/trmm-cli/"
	AUTHORS      = "David Randall"
	LICENSE      = "PostgreSQL"
	COPYRIGHT    = "(c) 2022 Nice Guy IT, LLC"
	ORGANIZATION = "Nice Guy IT, LLC"
	YEAR         = "2022"
)

// NewTacticalCLI starts a new Tactical CLI request.
func NewTacticalCLI() *TacticalCLI {
	t := TacticalCLI{}
	t.config = &TacticalConfig{}
	t.cli = NewCLI().ParseCLI()
	t.trmm = trmm.NewTacticalRMM()
	return &t
}

// SetDomain will set the API domain for Tactical RMM.
func (t *TacticalCLI) SetDomain(domain string) *TacticalCLI {
	t.trmm.SetDomain(domain)
	if t.trmm.HasErrors() {
		t.err = t.trmm.Error()
		return t
	}
	return t
}

// SetAPIKey will set the API key for Tactical RMM.
func (t *TacticalCLI) SetAPIKey(key string) *TacticalCLI {
	t.trmm.SetAPIKey(key)
	if t.trmm.HasErrors() {
		t.err = t.trmm.Error()
		return t
	}
	return t
}

// SetTimeout will set the HTTP timeout.
func (t *TacticalCLI) SetTimeout(seconds int) *TacticalCLI {
	t.trmm.SetTimeout(seconds)
	if t.trmm.HasErrors() {
		t.err = t.trmm.Error()
		return t
	}
	return t
}

// Run will run the Tactical CLI request.
func (t *TacticalCLI) Run() *TacticalCLI {
	if t.err != nil {
		return t
	}

	switch kongContext.Command() {
	// Accounts endpoint
	case "accounts users":
		fmt.Println(t.trmm.GetAccountsUsers().ToCsv(t.trmm.Accounts.Users).ToString())
	case "accounts roles":
		// TODO: Add a role to test this.
		fmt.Println(t.trmm.GetAccountsRoles().ToCsv(t.trmm.Accounts.Roles).ToString())
	case "accounts api-keys":
		fmt.Println(t.trmm.GetAccountsAPIKeys().ToCsv(t.trmm.Accounts.APIKeys).ToString())

	// Agents endpoint
	case "agents agents":
		fmt.Println(t.trmm.GetAgentsAgents().ToCsv(t.trmm.Agents.Agents).ToString())
	case "agents history":
		fmt.Println(t.trmm.GetAgentsHistory().ToCsv(t.trmm.Agents.History).ToString())
	case "agents notes":
		fmt.Println(t.trmm.GetAgentsNotes().ToCsv(t.trmm.Agents.Notes).ToString())
	case "agents versions":
		t.trmm.GetAgentsVersions()
		var arr = []trmm.AgentsVersions{*t.trmm.Agents.Versions}
		fmt.Println(t.trmm.ToCsv(arr).ToString())

	case "alerts templates":
		fmt.Println(t.trmm.GetAlertsTemplates().ToCsv(t.trmm.Alerts.Templates).ToString())

	// Checks endpoint
	case "checks":
		fmt.Println(t.trmm.GetChecks().ToCsv(t.trmm.Checks).ToString())

	// Clients endpoint
	case "clients clients":
		fmt.Println(t.trmm.GetClients().ToCsv(t.trmm.Clients).ToString())
	case "clients sites":
		t.trmm.GetClients()
		if *t.trmm.Clients != nil {
			for _, clients := range *t.trmm.Clients {
				fmt.Println(t.trmm.ToCsv(clients.Sites).ToString())
			}
		} else {
			fmt.Println("No clients sites found")
		}
	case "clients offboard <client>":
		force := false
		if len(kongContext.Args) > 3 {
			force = true
		}

		success, err := t.trmm.Offboard(kongContext.Args[2], force)
		if success {
			fmt.Printf("Successfully offboarded %s\n", kongContext.Args[2])
		} else {
			fmt.Printf("Could not offboard %s: %s\n", kongContext.Args[2], err)
		}

	// Core endpoint
	case "core code-sign":
		t.trmm.GetCoreCodeSign()
		var arr = []trmm.CoreCodeSign{*t.trmm.Core.CodeSign}
		fmt.Println(t.trmm.ToCsv(arr).ToString())
	case "core custom-fields":
		fmt.Println(t.trmm.GetCoreCustomFields().ToCsv(t.trmm.Core.CustomFields).ToString())
	case "core dash-info":
		t.trmm.GetCoreDashInfo()
		var arr = []trmm.CoreDashInfo{*t.trmm.Core.DashInfo}
		fmt.Println(t.trmm.ToCsv(arr).ToString())
	case "core key-store":
		fmt.Println(t.trmm.GetCoreKeyStore().ToCsv(t.trmm.Core.KeyStore).ToString())
	case "core settings":
		t.trmm.GetCoreSettings()
		// Don't need a list of all timezones.
		t.trmm.Core.Settings.AllTimezones = nil
		var arr = []trmm.CoreSettings{*t.trmm.Core.Settings}
		fmt.Println(t.trmm.ToCsv(arr).ToString())
	case "core version":
		t.trmm.GetCoreVersion()
		var arr = []trmm.CoreVersion{*t.trmm.Core.Version}
		fmt.Println(t.trmm.ToCsv(arr).ToString())

	// Logs endpoint
	case "logs pending-actions":
		fmt.Println(t.trmm.GetLogsPendingActions().ToCsv(t.trmm.Logs.PendingActions).ToString())

	// Scripts endpoint
	case "scripts scripts":
		fmt.Println(t.trmm.GetScripts().ToCsv(t.trmm.Scripts.Scripts).ToString())
	case "scripts snippets":
		fmt.Println(t.trmm.GetScriptsSnippets().ToCsv(t.trmm.Scripts.Snippets).ToString())

	case "services":
		t.printNotImplemented()

	// Software endpoint
	case "software software":
		fmt.Println(t.trmm.GetSoftware().ToCsv(t.trmm.Software.Software).ToString())
	case "software chocos":
		fmt.Println(t.trmm.GetSoftwareChocos().ToCsv(t.trmm.Software.Chocos).ToString())

	// Tasks endpoint
	case "tasks":
		fmt.Println(t.trmm.GetTasks().ToCsv(t.trmm.Tasks).ToString())

	case "winupdate":
		t.printNotImplemented()

	case "version":
		_ = t.printVersion()

	case "license":
		_ = t.printLicense()

	default:
		log.Info().
			Str("command", kongContext.Command()).
			Msg("Invalid TRMM CLI command")
	}

	return t
}

// HasErrors returns true if errors are present.
func (t *TacticalCLI) HasErrors() bool {
	return t.err != nil
}

// ErrorMessage returns the error message.
func (t *TacticalCLI) ErrorMessage() string {
	return t.err.Error()
}

// Errors returns the error if present, or nil otherwise.
func (t *TacticalCLI) Error() error {
	return t.err
}

// printVersion will print the version information
func (t *TacticalCLI) printVersion() error {
	log.Debug().
		Msg("Printing version information")
	tmpl := template.Must(template.New("version").Parse(`
Name:       {{.Name}}
Version:    {{.Version}}
Build Env:  {{.BuildEnv}}
Build Time: {{.BuildTime}}
Go Version: {{.GoVersion}}
Go OS/Arch: {{.GoOSArch}}
Git Tag:    {{.GitTag}}
Git Commit: {{.GitCommit}}
Git Branch: {{.GitBranch}}
URL:        {{.URL}}
Authors:    {{.Authors}}
License:    {{.License}}
Copyright:  {{.Copyright}}
`))
	buf := bytes.NewBuffer(nil)
	t.err = tmpl.Execute(buf, map[string]interface{}{
		"Name":      NAME,
		"Version":   VERSION,
		"BuildTime": BUILD_TIME,
		"BuildEnv":  BUILD_ENV,
		"GoVersion": runtime.Version(),
		"GoOSArch":  runtime.GOOS + "/" + runtime.GOARCH,
		"GitTag":    GIT_TAG,
		"GitCommit": GIT_COMMIT,
		"GitBranch": GIT_BRANCH,
		"URL":       URL,
		"Authors":   AUTHORS,
		"License":   LICENSE,
		"Copyright": COPYRIGHT,
	})
	if t.err != nil {
		log.Error().
			Err(t.err).
			Str("error", t.err.Error()).
			Msg("Error executing version template.")
		return t.err
	}
	fmt.Println(buf.String())
	return nil
}

// printLicense will print the license information
func (t *TacticalCLI) printLicense() error {
	log.Debug().
		Msg("Printing license information")
	tmpl := template.Must(template.New("version").Parse(`PostgreSQL License

Copyright (c) {{.Year}}, {{.Organization}}

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose, without fee, and without a written
agreement is hereby granted, provided that the above copyright notice
and this paragraph and the following two paragraphs appear in all
copies.

IN NO EVENT SHALL {{.Organization}} BE LIABLE TO ANY PARTY FOR DIRECT,
INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING
LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
DOCUMENTATION, EVEN IF {{.Organization}} HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

{{.Organization}} SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
AND {{.organization}} HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
`))
	buf := bytes.NewBuffer(nil)
	t.err = tmpl.Execute(buf, map[string]interface{}{
		"Organization": ORGANIZATION,
		"Year":         YEAR,
	})
	if t.err != nil {
		log.Error().
			Err(t.err).
			Str("error", t.err.Error()).
			Msg("Error executing version template.")
		return t.err
	}
	fmt.Println(buf.String())
	return nil
}

// printNotImplemented will print a message that the trmm-cli command is not implemented
func (t *TacticalCLI) printNotImplemented() {
	log.Debug().
		Msg("Printing the Not Implemented message")
	fmt.Println("This command is not implemented")
}
