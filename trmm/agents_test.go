package trmm

import (
	"os"
	"testing"
)

func init() {
	rmm = NewTacticalRMM().
		SetDomain(os.Getenv("TRMM_API_DOMAIN")).
		SetAPIKey(os.Getenv("TRMM_API_KEY"))
}

func TestGetAgentsClient(t *testing.T) {
	rmm.GetAgentsClient("Hoth")
	if len(*rmm.Agents.Agents) == 0 {
		t.Fatal("Found no agents")
	}

	t.Logf("Found %d agents.", len(*rmm.Agents.Agents))
}

func TestDeleteAgent(t *testing.T) {
	t.Log("Getting all agents and deleting the first one")
	rmm.GetAgentsAgents()
	rmm.DeleteAgent((*rmm.Agents.Agents)[0].AgentId)
	if rmm.err != nil {
		t.Fatalf("%s %s", rmm.response, rmm.err)
	}

	t.Log("Deleted agent")
}
