package trmm

import (
	"os"
	"testing"
)

func init() {
	rmm = NewTacticalRMM().
		SetDomain(os.Getenv("TRMM_API_DOMAIN")).
		SetAPIKey(os.Getenv("TRMM_API_KEY"))
}

func TestGetClients(t *testing.T) {
	rmm.GetClients()
	if rmm.Clients == nil {
		t.Fatal("Could not get clients")
	}

	t.Logf("Got %d client(s)", len(*rmm.Clients))
}

func TestGetClient(t *testing.T) {
	rmm.GetClient("Delete")
	if rmm.Client.Name == "" {
		t.Fatal("Could not get client")
	}

	t.Logf("Got client %s", rmm.Client.Name)
}

func TestRenameClient(t *testing.T) {
	rmm.GetClients()
	oldName := (*rmm.Clients)[0].Name
	t.Logf("Renaming client: %s", (*rmm.Clients)[0].Name)
	rmm.RenameClient((*rmm.Clients)[0].Name, "Renamed")
	rmm.GetClient("Renamed")
	if rmm.Client.Name != "Renamed" {
		t.Fatal("Could not rename first client")
	}

	rmm.RenameClient("Renamed", oldName)
	t.Log("Renamed client success")
}

func TestCreateClient(t *testing.T) {
	t.Log("Creating Test client")
	rmm.CreateClient("Test", "main")
	t.Log(rmm.response)
	if rmm.err != nil {
		t.Fatalf("%s: %s", rmm.response, rmm.err)
	}

	t.Log("Created client Test")
}

func TestDeleteClient(t *testing.T) {
	t.Log("Creating delete client")
	rmm.CreateClient("Delete", "main")
	t.Log("Deleting client Delete")
	rmm.DeleteClient("Delete")
	if rmm.err != nil {
		t.Fatal(rmm.err)
	}

	rmm.GetClient("Delete")
	t.Log("Client deleted")
}

func TestOffboardClient(t *testing.T) {
	t.Log("Offboarding client Offboard")
	result, err := rmm.Offboard("Offboard", false)
	if !result {
		t.Fatalf("Unable to offboard: %s", err)
	}

	t.Log("Offboarded the client Offboard")
}

func TestOffboardClientForce(t *testing.T) {
	t.Log("Offboarding client Offboard with force")
	result, err := rmm.Offboard("Offboard", true)
	if !result {
		t.Fatalf("Unable to offboard: %s", err)
	}

	t.Log("Offboarded the client Offboard with force")
}
