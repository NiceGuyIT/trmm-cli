package trmm

type Client struct {
	Id                     int                       `json:"id" csv:"id"`
	Name                   string                    `json:"name" csv:"name"`
	ServerPolicy           interface{}               `json:"server_policy" csv:"server_policy"`
	WorkstationPolicy      interface{}               `json:"workstation_policy" csv:"workstation_policy"`
	AlertTemplate          interface{}               `json:"alert_template" csv:"alert_template"`
	BlockPolicyInheritance bool                      `json:"block_policy_inheritance" csv:"block_policy_inheritance"`
	Sites                  []ClientSites            `json:"sites" csv:"sites"`
	CustomFields           []interface{}             `json:"custom_fields" csv:"custom_fields"`
	AgentCount             int                       `json:"agent_count" csv:"agent_count"`
	MaintenanceMode        bool                      `json:"maintenance_mode" csv:"maintenance_mode"`
	FailingChecks          ClientsSitesFailingChecks `json:"failing_checks" csv:"failing_checks"`
}

type ClientSites struct {
	Id                     int                       `json:"id" csv:"id"`
	Name                   string                    `json:"name" csv:"name"`
	ServerPolicy           interface{}               `json:"server_policy" csv:"server_policy"`
	WorkstationPolicy      interface{}               `json:"workstation_policy" csv:"workstation_policy"`
	AlertTemplate          interface{}               `json:"alert_template" csv:"alert_template"`
	ClientName             string                    `json:"client_name" csv:"client_name"`
	Client                 int                       `json:"client" csv:"client"`
	CustomFields           []interface{}             `json:"custom_fields" csv:"custom_fields"`
	AgentCount             int                       `json:"agent_count" csv:"agent_count"`
	BlockPolicyInheritance bool                      `json:"block_policy_inheritance" csv:"block_policy_inheritance"`
	MaintenanceMode        bool                      `json:"maintenance_mode" csv:"maintenance_mode"`
	FailingChecks          ClientsSitesFailingChecks `json:"failing_checks" csv:"failing_checks"`
}

type ClientsSitesFailingChecks struct {
	Error   bool `json:"error"`
	Warning bool `json:"warning"`
}

type ClientWrapper struct {
	Client Client `json:"client"`
}

type CreateClientWrapper struct {
	Client CreateClient `json:"client"`
	Site CreateSite `json:"site"`
	CustomFields []string `json:"custom_fiels"`
}

type CreateClient struct {
	Name string `json:"name"`
}

type CreateSite struct {
	Name string `json:"name"`
}
