// Package trmm integrates with the Tactical RMM API.
package trmm

// Software represents all Software endpoints.
type Software struct {
	Software *[]SoftwareByAgent
	Chocos   *[]SoftwareChocos
}

type SoftwareByAgent struct {
	Id       int                `json:"id" csv:"id"`
	Software []SoftwareSoftware `json:"software" csv:"software"`
	Agent    int                `json:"agent" csv:"agent"`
}

type SoftwareSoftware struct {
	Name        string `json:"name" csv:"name"`
	Version     string `json:"version" csv:"version"`
	Publisher   string `json:"publisher" csv:"publisher"`
	Source      string `json:"source" csv:"source"`
	Location    string `json:"location" csv:"location"`
	Size        string `json:"size" csv:"size"`
	InstallDate string `json:"install_date" csv:"install_date"`
	Uninstall   string `json:"uninstall" csv:"uninstall"`
}

type SoftwareChocos struct {
	Name string `json:"name" csv:"name"`
}

// GetSoftware will get the Software from the TRMM API.
func (t *TRMM) GetSoftware() *TRMM {
	if t.err != nil {
		return t
	}

	t.Software.Software = t.setURL("software").
		apiGet([]SoftwareByAgent{}).
		response.Result().(*[]SoftwareByAgent)
	return t
}

// GetSoftwareChocos will get the SoftwareChocos from the TRMM API.
func (t *TRMM) GetSoftwareChocos() *TRMM {
	if t.err != nil {
		return t
	}

	t.Software.Chocos = t.setURL("software/chocos").
		apiGet([]SoftwareChocos{}).
		response.Result().(*[]SoftwareChocos)
	return t
}
