// Package trmm integrates with the Tactical RMM API.
package trmm

// Scripts represents all Scripts endpoints.
type Scripts struct {
	Scripts  *[]ScriptsScripts
	Snippets *[]ScriptsSnippets
}

type ScriptsScripts struct {
	Id                 int           `json:"id"`
	Name               string        `json:"name"`
	Description        string        `json:"description"`
	ScriptType         string        `json:"script_type"`
	Shell              string        `json:"shell"`
	Args               []interface{} `json:"args"`
	Category           string        `json:"category"`
	Favorite           bool          `json:"favorite"`
	DefaultTimeout     int           `json:"default_timeout"`
	Syntax             interface{}   `json:"syntax"`
	Filename           interface{}   `json:"filename"`
	Hidden             bool          `json:"hidden"`
	SupportedPlatforms []interface{} `json:"supported_platforms"`
}

type ScriptsSnippets struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Desc  string `json:"desc"`
	Code  string `json:"code"`
	Shell string `json:"shell"`
}

// GetScripts will get the Scripts from the TRMM API.
func (t *TRMM) GetScripts() *TRMM {
	if t.err != nil {
		return t
	}

	t.Scripts.Scripts = t.setURL("scripts").
		apiGet([]ScriptsScripts{}).
		response.Result().(*[]ScriptsScripts)
	return t
}

// GetScriptsSnippets will get the ScriptsSnippets from the TRMM API.
func (t *TRMM) GetScriptsSnippets() *TRMM {
	if t.err != nil {
		return t
	}

	t.Scripts.Snippets = t.setURL("scripts").
		apiGet([]ScriptsSnippets{}).
		response.Result().(*[]ScriptsSnippets)
	return t
}
