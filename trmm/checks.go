// Package trmm integrates with the Tactical RMM API.
package trmm

type Checks struct {
	Id                    int           `json:"id"`
	ReadableDesc          string        `json:"readable_desc"`
	AssignedTask          interface{}   `json:"assigned_task"`
	LastRun               interface{}   `json:"last_run"`
	HistoryInfo           interface{}   `json:"history_info"`
	AlertTemplate         interface{}   `json:"alert_template"`
	CreatedBy             string        `json:"created_by"`
	CreatedTime           string        `json:"created_time"`
	ModifiedBy            string        `json:"modified_by"`
	ModifiedTime          string        `json:"modified_time"`
	ManagedByPolicy       bool          `json:"managed_by_policy"`
	OverridenByPolicy     bool          `json:"overriden_by_policy"`
	ParentCheck           interface{}   `json:"parent_check"`
	Name                  interface{}   `json:"name"`
	CheckType             string        `json:"check_type"`
	Status                string        `json:"status"`
	MoreInfo              interface{}   `json:"more_info"`
	EmailAlert            bool          `json:"email_alert"`
	TextAlert             bool          `json:"text_alert"`
	DashboardAlert        bool          `json:"dashboard_alert"`
	FailsB4Alert          int           `json:"fails_b4_alert"`
	FailCount             int           `json:"fail_count"`
	OutageHistory         interface{}   `json:"outage_history"`
	ExtraDetails          interface{}   `json:"extra_details"`
	RunInterval           int           `json:"run_interval"`
	AlertSeverity         string        `json:"alert_severity"`
	ErrorThreshold        int           `json:"error_threshold"`
	WarningThreshold      int           `json:"warning_threshold"`
	Disk                  string        `json:"disk"`
	Ip                    interface{}   `json:"ip"`
	ScriptArgs            []interface{} `json:"script_args"`
	InfoReturnCodes       []interface{} `json:"info_return_codes"`
	WarningReturnCodes    []interface{} `json:"warning_return_codes"`
	Timeout               interface{}   `json:"timeout"`
	Stdout                interface{}   `json:"stdout"`
	Stderr                interface{}   `json:"stderr"`
	Retcode               interface{}   `json:"retcode"`
	ExecutionTime         interface{}   `json:"execution_time"`
	History               []interface{} `json:"history"`
	SvcName               interface{}   `json:"svc_name"`
	SvcDisplayName        interface{}   `json:"svc_display_name"`
	PassIfStartPending    interface{}   `json:"pass_if_start_pending"`
	PassIfSvcNotExist     bool          `json:"pass_if_svc_not_exist"`
	RestartIfStopped      interface{}   `json:"restart_if_stopped"`
	SvcPolicyMode         interface{}   `json:"svc_policy_mode"`
	LogName               interface{}   `json:"log_name"`
	EventId               interface{}   `json:"event_id"`
	EventIdIsWildcard     bool          `json:"event_id_is_wildcard"`
	EventType             interface{}   `json:"event_type"`
	EventSource           interface{}   `json:"event_source"`
	EventMessage          interface{}   `json:"event_message"`
	FailWhen              interface{}   `json:"fail_when"`
	SearchLastDays        interface{}   `json:"search_last_days"`
	NumberOfEventsB4Alert int           `json:"number_of_events_b4_alert"`
	Agent                 interface{}   `json:"agent"`
	Policy                int           `json:"policy"`
	Script                interface{}   `json:"script"`
}

// GetChecks will get the Checks from the TRMM API.
func (t *TRMM) GetChecks() *TRMM {
	if t.err != nil {
		return t
	}

	t.Checks = t.setURL("checks").
		apiGet([]Checks{}).
		response.Result().(*[]Checks)
	return t
}
