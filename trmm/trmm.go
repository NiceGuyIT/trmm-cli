package trmm

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/gocarina/gocsv"
	"github.com/rs/zerolog/log"
	"time"
)

type TRMM struct {
	baseURL   string
	apiDomain string
	apiKey    string
	apiURL    string
	timeout   time.Duration `default:"10"`
	resty     *resty.Client
	response  *resty.Response
	httpCode  string
	output    string
	bytes     []byte
	err       error
	// List of top-level TRMM endpoints
	Accounts Accounts
	Agents   Agents
	// The Alerts endpoint returns a list of alert Templates.
	Alerts    Alerts
	Checks    *[]Checks
	Clients   *[]Client
	Client	  *Client
	Core      Core
	Logs      Logs
	Scripts   Scripts
	Services  *[]Services
	Software  Software
	Tasks     *[]Tasks
	Winupdate *[]Winupdate
}

const (
	API_HEADER  = "X-API-KEY"
	API_TIMEOUT = 10
)

// NewTacticalRMM starts a new TRMM request.
func NewTacticalRMM() *TRMM {
	tactical := TRMM{
		// This does not populate from the default specified in the struct
		timeout: time.Second * time.Duration(API_TIMEOUT),
		resty:   resty.New(),
	}
	tactical.resty.SetTimeout(tactical.timeout)
	return &tactical
}

// SetDomain will set the API domain for Tactical RMM.
func (t *TRMM) SetDomain(domain string) *TRMM {
	if domain == "" {
		t.err = errors.New("domain is not set")
		log.Error().
			Err(t.err).
			Msg("TRMM domain is not set.")
		return t
	}
	t.apiDomain = domain
	t.baseURL = fmt.Sprintf("https://%s", t.apiDomain)
	return t
}

// SetAPIKey will set the API key for Tactical RMM.
func (t *TRMM) SetAPIKey(key string) *TRMM {
	if key == "" {
		t.err = errors.New("api key is not set")
		log.Error().
			Err(t.err).
			Msg("TRMM API key is not set.")
		return t
	}
	t.apiKey = key
	t.resty.SetHeaders(map[string]string{
		API_HEADER: t.apiKey,
	})
	return t
}

// SetTimeout will set the HTTP timeout. Use 0 for no timeout. Do not use 0. Always specify a timeout.
func (t *TRMM) SetTimeout(seconds int) *TRMM {
	if seconds < 0 {
		log.Warn().
			Int("seconds", seconds).
			Msg("HTTP timeout is less than 0.")
		return t
	}
	if seconds > 600 {
		log.Warn().
			Int("seconds", seconds).
			Msg("HTTP timeout is set too high. Using 10 minutes (600 seconds).")
		seconds = 600
	}
	t.timeout = time.Second * time.Duration(seconds)
	t.resty.SetTimeout(t.timeout)
	return t
}

// setURL will set the URL for an HTTP operation.
func (t *TRMM) setURL(url string) *TRMM {
	if t.err != nil {
		return t
	}

	if url == "" {
		t.err = errors.New("api URL is empty")
		log.Error().
			Err(t.err).
			Str("url", url).
			Msg("Failed to set the API URL.")
		return t
	}

	t.apiURL = fmt.Sprintf("%s/%s", t.baseURL, url)
	return t
}

func (t *TRMM) apiPut(apiInterface interface{}) *TRMM {
	if t.err != nil {
		return t
	}

	t.response, t.err = t.resty.R().SetBody(apiInterface).Put(t.apiURL)
	if t.err != nil {
		log.Error().
			Err(t.err).
			Str("http_code", t.response.Status()).
			Msg("Error with HTTP POST request")
	}

	return t
}

func (t *TRMM) apiDelete() *TRMM {
	if t.err != nil {
		return t
	}

	t.response, t.err = t.resty.R().Delete(t.apiURL)
	if t.err != nil {
		log.Error().
			Err(t.err).
			Str("http_code", t.response.Status()).
			Msg("Error with HTTP POST request")
	}

	return t
}

func (t *TRMM) apiPost(apiInterface interface{}) *TRMM {
	if t.err != nil {
		return t
	}

	//if apiInterface == nil {
	//	t.response, t.err = t.resty.R().SetHeader("Content-Type", "application/json").Post(t.apiURL)
	//} else {
	//	t.response, t.err = t.resty.R().SetResult(&apiInterface).Post(t.apiURL)
	//}

	t.response, t.err = t.resty.R().SetBody(apiInterface).Post(t.apiURL)

	if t.err != nil {
		log.Error().
			Err(t.err).
			Str("http_code", t.response.Status()).
			Msg("Error with HTTP POST request")
	}

	return t
}

// apiGet will perform a GET to the API.
func (t *TRMM) apiGet(apiInterface interface{}) *TRMM {
	if t.err != nil {
		return t
	}

	if apiInterface == nil {
		t.response, t.err = t.resty.R().Get(t.apiURL)
	} else {
		t.response, t.err = t.resty.R().SetResult(apiInterface).Get(t.apiURL)
	}
	if t.err != nil {
		log.Error().
			Err(t.err).
			Str("http_code", t.response.Status()).
			Msg("Error with HTTP GET request")
		return t
	}

	/*
		fmt.Println(log.Logger.GetLevel().String())
		fmt.Println(log.Logger.GetLevel())
		log.Warn().
			Int8("log_level", int8(log.Logger.GetLevel())).
			Str("log_level", fmt.Sprintf("%#v", log.Logger.GetLevel().String())).
			Msg("Log level within program.")
	*/

	// Dump the raw JSON response when debugging.
	// FIXME: This doesn't work as intended. GetLevel() always returns "trace"
	/*
		if log.Logger.GetLevel().String() == "debug" || log.Logger.GetLevel().String() == "trace" {
			log.Warn().
				Str("log_level", log.Logger.GetLevel().String()).
				Msg("Log level")
			fmt.Println("Raw JSON output from API:")
			fmt.Println(t.response.String())
			fmt.Println()
		}
	*/

	return t
}

// ToJSON will convert the given TRMM struct to JSON.
func (t *TRMM) ToJSON(trmmStruct interface{}) *TRMM {
	if t.err != nil {
		return t
	}

	t.bytes, t.err = json.MarshalIndent(trmmStruct, "", "  ")
	if t.err != nil {
		log.Error().
			Err(t.err).
			Str("trmm_struct", fmt.Sprintf("%#v", trmmStruct)).
			Msg("Failed to marshal TRMM struct to JSON.")
		return t
	}

	return t
}

// ToCsv will convert the given TRMM struct slice to CSV
func (t *TRMM) ToCsv(trmmStruct interface{}) *TRMM {
	if t.err != nil {
		return t
	}

	t.bytes, t.err = gocsv.MarshalBytes(trmmStruct)
	if t.err != nil {
		log.Error().
			Err(t.err).
			Str("trmm_struct", fmt.Sprintf("%#v", trmmStruct)).
			Msg("Failed to marshal TRMM struct to CSV.")
		return t
	}
	return t
}

// ValueToCsv will convert the given TRMM struct (single value) to CSV.
// FIXME: Use reflection to convert a struct into a slice.
func (t *TRMM) ValueToCsv(trmmStruct interface{}) *TRMM {
	if t.err != nil {
		return t
	}

	t.bytes, t.err = gocsv.MarshalBytes(trmmStruct)
	if t.err != nil {
		log.Error().
			Err(t.err).
			Str("trmm_struct", fmt.Sprintf("%#v", trmmStruct)).
			Msg("Failed to marshal TRMM struct to CSV.")
		return t
	}
	return t
}

// ToString will return the TRMM structure as a string.
func (t *TRMM) ToString() string {
	if t.err != nil {
		return ""
	}

	t.ToBytes()
	if t.err != nil {
		return ""
	}

	return string(t.bytes)
}

// ToBytes will convert the TRMM structure to a []byte slice.
func (t *TRMM) ToBytes() []byte {
	if t.err != nil {
		return nil
	}

	if len(t.bytes) == 0 {
		if t.err != nil {
			return nil
		}
	}

	return t.bytes
}

// HasErrors returns true if errors are present.
func (t *TRMM) HasErrors() bool {
	return t.err != nil
}

// ErrorMessage returns the error message.
func (t *TRMM) ErrorMessage() string {
	return t.err.Error()
}

// Errors returns the error if present, or nil otherwise.
func (t *TRMM) Error() error {
	return t.err
}
