// Package trmm integrates with the Tactical RMM API.
package trmm

// Accounts is not an endpoint, rather it gathers all the Accounts* endpoints into a single struct.
type Accounts struct {
	APIKeys *[]AccountsAPIKeys
	Roles   *[]AccountsRoles
	Users   *[]AccountsUsers
}

type AccountsAPIKeys struct {
	Id           int         `json:"id"`
	Username     string      `json:"username"`
	CreatedBy    string      `json:"created_by"`
	CreatedTime  string      `json:"created_time"`
	ModifiedBy   string      `json:"modified_by"`
	ModifiedTime string      `json:"modified_time"`
	Name         string      `json:"name"`
	Key          string      `json:"key"`
	Expiration   interface{} `json:"expiration"`
	User         int         `json:"user"`
}

type AccountsRoles struct {
	Id int `json:"id"`
}

type AccountsUsers struct {
	Id                  int         `json:"id"`
	Username            string      `json:"username"`
	FirstName           string      `json:"first_name"`
	LastName            string      `json:"last_name"`
	Email               string      `json:"email"`
	IsActive            bool        `json:"is_active"`
	LastLogin           string      `json:"last_login"`
	LastLoginIp         string      `json:"last_login_ip"`
	Role                interface{} `json:"role"`
	BlockDashboardLogin bool        `json:"block_dashboard_login"`
}

// GetAccountsUsers will get the AccountsUsers from the TRMM API.
func (t *TRMM) GetAccountsUsers() *TRMM {
	if t.err != nil {
		return t
	}

	t.Accounts.Users = t.setURL("accounts/users").
		apiGet([]AccountsUsers{}).
		response.Result().(*[]AccountsUsers)
	return t
}

// GetAccountsRoles will get the AccountsRoles from the TRMM API.
func (t *TRMM) GetAccountsRoles() *TRMM {
	if t.err != nil {
		return t
	}

	t.Accounts.Roles = t.setURL("accounts/roles").
		apiGet([]AccountsRoles{}).
		response.Result().(*[]AccountsRoles)
	return t
}

// GetAccountsAPIKeys will get the AccountsAPIKeys from the TRMM API.
func (t *TRMM) GetAccountsAPIKeys() *TRMM {
	if t.err != nil {
		return t
	}

	t.Accounts.APIKeys = t.setURL("accounts/apikeys").
		apiGet([]AccountsAPIKeys{}).
		response.Result().(*[]AccountsAPIKeys)
	return t
}
