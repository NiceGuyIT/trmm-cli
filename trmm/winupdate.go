// Package trmm integrates with the Tactical RMM API.
package trmm

type Winupdate struct {
	Id             int      `json:"id"`
	DateInstalled  *string  `json:"date_installed"`
	Guid           string   `json:"guid"`
	Kb             string   `json:"kb"`
	Title          string   `json:"title"`
	Installed      bool     `json:"installed"`
	Downloaded     bool     `json:"downloaded"`
	Description    string   `json:"description"`
	Severity       string   `json:"severity"`
	Categories     []string `json:"categories"`
	CategoryIds    []string `json:"category_ids"`
	KbArticleIds   []string `json:"kb_article_ids"`
	MoreInfoUrls   []string `json:"more_info_urls"`
	SupportUrl     string   `json:"support_url"`
	RevisionNumber int      `json:"revision_number"`
	Action         string   `json:"action"`
	Result         string   `json:"result"`
	Agent          int      `json:"agent"`
}
