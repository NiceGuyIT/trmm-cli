package trmm

import (
	"time"
)

type Agents struct {
	Agents   *[]AgentsAgents
	History  *[]AgentsHistory
	Notes    *[]AgentsNotes
	Versions *AgentsVersions
}

type AgentsAgents struct {
	AgentId                string       `json:"agent_id" csv:"agent_id"`
	AlertTemplate          interface{}  `json:"alert_template" csv:"-"`
	BlockPolicyInheritance bool         `json:"block_policy_inheritance" csv:"block_policy_inheritance"`
	BootTime               float64      `json:"boot_time,float64" csv:"boot_time"`
	Checks                 AgentsChecks `json:"checks" csv:"-"`
	ClientName             string       `json:"client_name" csv:"client_name"`
	Description            string       `json:"description" csv:"description"`
	Goarch                 string       `json:"goarch" csv:"goarch"`
	HasPatchesPending      bool         `json:"has_patches_pending" csv:"has_patches_pending"`
	Hostname               string       `json:"hostname" csv:"hostname"`
	Italic                 bool         `json:"italic" csv:"italic"`
	LastSeen               string       `json:"last_seen" csv:"last_seen"`
	LoggedUsername         string       `json:"logged_username" csv:"logged_username"`
	MaintenanceMode        bool         `json:"maintenance_mode" csv:"maintenance_mode"`
	MonitoringType         string       `json:"monitoring_type" csv:"monitoring_type"`
	NeedsReboot            bool         `json:"needs_reboot" csv:"needs_reboot"`
	OverdueDashboardAlert  bool         `json:"overdue_dashboard_alert" csv:"overdue_dashboard_alert"`
	OverdueEmailAlert      bool         `json:"overdue_email_alert" csv:"overdue_email_alert"`
	OverdueTextAlert       bool         `json:"overdue_text_alert" csv:"overdue_text_alert"`
	PendingActionsCount    int          `json:"pending_actions_count" csv:"pending_actions_count"`
	Plat                   string       `json:"plat" csv:"plat"`
	SiteName               string       `json:"site_name" csv:"site_name"`
	Status                 string       `json:"status" csv:"status"`
}

type AgentsChecks struct {
	Failing          int  `json:"failing"`
	HasFailingChecks bool `json:"has_failing_checks"`
	Info             int  `json:"info"`
	Passing          int  `json:"passing"`
	Total            int  `json:"total"`
	Warning          int  `json:"warning"`
}

type AgentsHistory struct {
	Id            int                 `json:"id" csv:"id"`
	Time          time.Time           `json:"time" csv:"time"`
	Type          string              `json:"type" csv:"type"`
	Command       string              `json:"command" csv:"command"`
	Status        string              `json:"status" csv:"status"`
	Username      string              `json:"username" csv:"username"`
	Results       interface{}         `json:"results" csv:"results"`
	ScriptResults AgentsScriptResults `json:"script_results" csv:"script_results"`
	Agent         int                 `json:"agent" csv:"agent"`
	Script        interface{}         `json:"script" csv:"script"`
}

type AgentsScriptResults struct {
	Id            int     `json:"id"`
	Stderr        string  `json:"stderr"`
	Stdout        string  `json:"stdout"`
	Retcode       int     `json:"retcode"`
	ExecutionTime float64 `json:"execution_time"`
}

type AgentsNotes struct {
	Pk        int       `json:"pk" csv:"pk"`
	EntryTime time.Time `json:"entry_time" csv:"entry_time"`
	Note      string    `json:"note" csv:"note"`
	Username  string    `json:"username" csv:"username"`
	AgentId   string    `json:"agent_id" csv:"agent_id"`
}

type AgentsVersions struct {
	Versions []string               `json:"versions" csv:"versions"`
	Agents   []AgentsVersionsAgents `json:"agents" csv:"agents"`
}

type AgentsVersionsAgents struct {
	Id       int    `json:"id" csv:"id"`
	Hostname string `json:"hostname" csv:"hostname"`
	AgentId  string `json:"agent_id" csv:"agent_id"`
	Client   string `json:"client" csv:"client"`
	Site     string `json:"site" csv:"site"`
}

type AgentsId struct {
	Winupdatepolicy       []AgentsIdWinupdatepolicy `json:"winupdatepolicy"`
	Status                string                    `json:"status"`
	CpuModel              []string                  `json:"cpu_model"`
	LocalIps              string                    `json:"local_ips"`
	MakeModel             string                    `json:"make_model"`
	PhysicalDisks         []string                  `json:"physical_disks"`
	Graphics              string                    `json:"graphics"`
	Checks                AgentsIdChecks            `json:"checks"`
	Timezone              string                    `json:"timezone"`
	AllTimezones          []string                  `json:"all_timezones"`
	Client                string                    `json:"client"`
	SiteName              string                    `json:"site_name"`
	CustomFields          []interface{}             `json:"custom_fields"`
	PatchesLastInstalled  interface{}               `json:"patches_last_installed"`
	LastSeen              time.Time                 `json:"last_seen"`
	AppliedPolicies       AppliedPolicies           `json:"applied_policies"`
	EffectivePatchPolicy  EffectivePatchPolicy      `json:"effective_patch_policy"`
	AlertTemplate         interface{}               `json:"alert_template"`
	CreatedBy             interface{}               `json:"created_by"`
	CreatedTime           string                    `json:"created_time"`
	ModifiedBy            interface{}               `json:"modified_by"`
	ModifiedTime          string                    `json:"modified_time"`
	Version               string                    `json:"version"`
	OperatingSystem       string                    `json:"operating_system"`
	Plat                  string                    `json:"plat"`
	Goarch                string                    `json:"goarch"`
	PlatRelease           interface{}               `json:"plat_release"`
	Hostname              string                    `json:"hostname"`
	AgentId               string                    `json:"agent_id"`
	Services              []Services                `json:"services"`
	PublicIp              string                    `json:"public_ip"`
	TotalRam              int                       `json:"total_ram"`
	Disks                 []Disks                   `json:"disks"`
	BootTime              int                       `json:"boot_time"`
	LoggedInUsername      string                    `json:"logged_in_username"`
	LastLoggedInUser      string                    `json:"last_logged_in_user"`
	MonitoringType        string                    `json:"monitoring_type"`
	Description           string                    `json:"description"`
	MeshNodeId            string                    `json:"mesh_node_id"`
	OverdueEmailAlert     bool                      `json:"overdue_email_alert"`
	OverdueTextAlert      bool                      `json:"overdue_text_alert"`
	OverdueDashboardAlert bool                      `json:"overdue_dashboard_alert"`
	OfflineTime           int                       `json:"offline_time"`
	OverdueTime           int                       `json:"overdue_time"`
	CheckInterval         int                       `json:"check_interval"`
	NeedsReboot           bool                      `json:"needs_reboot"`
	ChocoInstalled        bool                      `json:"choco_installed"`
	WmiDetail             struct {
		Os [][]struct {
			Name                       string    `json:"Name"`
			Debug                      bool      `json:"Debug"`
			CSName                     string    `json:"CSName"`
			Locale                     string    `json:"Locale"`
			OSType                     int       `json:"OSType"`
			Status                     string    `json:"Status"`
			Caption                    string    `json:"Caption"`
			CodeSet                    string    `json:"CodeSet"`
			Primary                    bool      `json:"Primary"`
			Version                    string    `json:"Version"`
			BuildType                  string    `json:"BuildType"`
			SuiteMask                  int       `json:"SuiteMask"`
			BootDevice                 string    `json:"BootDevice"`
			CSDVersion                 string    `json:"CSDVersion"`
			OSLanguage                 int       `json:"OSLanguage"`
			PAEEnabled                 bool      `json:"PAEEnabled"`
			BuildNumber                string    `json:"BuildNumber"`
			CountryCode                string    `json:"CountryCode"`
			Description                string    `json:"Description"`
			Distributed                bool      `json:"Distributed"`
			InstallDate                time.Time `json:"InstallDate"`
			ProductType                int       `json:"ProductType"`
			SystemDrive                string    `json:"SystemDrive"`
			MUILanguages               []string  `json:"MUILanguages"`
			Manufacturer               string    `json:"Manufacturer"`
			Organization               string    `json:"Organization"`
			SerialNumber               string    `json:"SerialNumber"`
			SystemDevice               string    `json:"SystemDevice"`
			LocalDateTime              time.Time `json:"LocalDateTime"`
			NumberOfUsers              int       `json:"NumberOfUsers"`
			PlusProductID              string    `json:"PlusProductID"`
			LastBootUpTime             time.Time `json:"LastBootUpTime"`
			OSArchitecture             string    `json:"OSArchitecture"`
			OSProductSuite             int       `json:"OSProductSuite"`
			RegisteredUser             string    `json:"RegisteredUser"`
			CurrentTimeZone            int       `json:"CurrentTimeZone"`
			EncryptionLevel            int       `json:"EncryptionLevel"`
			SystemDirectory            string    `json:"SystemDirectory"`
			WindowsDirectory           string    `json:"WindowsDirectory"`
			CreationClassName          string    `json:"CreationClassName"`
			FreeVirtualMemory          int       `json:"FreeVirtualMemory"`
			NumberOfProcesses          int       `json:"NumberOfProcesses"`
			PlusVersionNumber          string    `json:"PlusVersionNumber"`
			FreePhysicalMemory         int       `json:"FreePhysicalMemory"`
			OperatingSystemSKU         int       `json:"OperatingSystemSKU"`
			TotalSwapSpaceSize         int       `json:"TotalSwapSpaceSize"`
			CSCreationClassName        string    `json:"CSCreationClassName"`
			MaxNumberOfProcesses       int64     `json:"MaxNumberOfProcesses"`
			MaxProcessMemorySize       int64     `json:"MaxProcessMemorySize"`
			OtherTypeDescription       string    `json:"OtherTypeDescription"`
			NumberOfLicensedUsers      int       `json:"NumberOfLicensedUsers"`
			FreeSpaceInPagingFiles     int       `json:"FreeSpaceInPagingFiles"`
			TotalVirtualMemorySize     int       `json:"TotalVirtualMemorySize"`
			TotalVisibleMemorySize     int       `json:"TotalVisibleMemorySize"`
			ServicePackMajorVersion    int       `json:"ServicePackMajorVersion"`
			ServicePackMinorVersion    int       `json:"ServicePackMinorVersion"`
			SizeStoredInPagingFiles    int       `json:"SizeStoredInPagingFiles"`
			ForegroundApplicationBoost int       `json:"ForegroundApplicationBoost"`
		} `json:"os"`
		Cpu [][]struct {
			Name                                    string    `json:"Name"`
			Role                                    string    `json:"Role"`
			Level                                   int       `json:"Level"`
			Family                                  int       `json:"Family"`
			Status                                  string    `json:"Status"`
			Caption                                 string    `json:"Caption"`
			Version                                 string    `json:"Version"`
			AssetTag                                string    `json:"AssetTag"`
			DeviceID                                string    `json:"DeviceID"`
			ExtClock                                int       `json:"ExtClock"`
			Revision                                int       `json:"Revision"`
			Stepping                                string    `json:"Stepping"`
			UniqueId                                string    `json:"UniqueId"`
			CpuStatus                               int       `json:"CpuStatus"`
			DataWidth                               int       `json:"DataWidth"`
			PartNumber                              string    `json:"PartNumber"`
			StatusInfo                              int       `json:"StatusInfo"`
			SystemName                              string    `json:"SystemName"`
			Description                             string    `json:"Description"`
			InstallDate                             time.Time `json:"InstallDate"`
			L2CacheSize                             int       `json:"L2CacheSize"`
			L3CacheSize                             int       `json:"L3CacheSize"`
			PNPDeviceID                             string    `json:"PNPDeviceID"`
			ProcessorId                             string    `json:"ProcessorId"`
			ThreadCount                             int       `json:"ThreadCount"`
			VoltageCaps                             int       `json:"VoltageCaps"`
			AddressWidth                            int       `json:"AddressWidth"`
			Architecture                            int       `json:"Architecture"`
			Availability                            int       `json:"Availability"`
			ErrorCleared                            bool      `json:"ErrorCleared"`
			L2CacheSpeed                            int       `json:"L2CacheSpeed"`
			L3CacheSpeed                            int       `json:"L3CacheSpeed"`
			Manufacturer                            string    `json:"Manufacturer"`
			SerialNumber                            string    `json:"SerialNumber"`
			LastErrorCode                           int       `json:"LastErrorCode"`
			MaxClockSpeed                           int       `json:"MaxClockSpeed"`
			NumberOfCores                           int       `json:"NumberOfCores"`
			ProcessorType                           int       `json:"ProcessorType"`
			UpgradeMethod                           int       `json:"UpgradeMethod"`
			CurrentVoltage                          int       `json:"CurrentVoltage"`
			LoadPercentage                          int       `json:"LoadPercentage"`
			Characteristics                         int       `json:"Characteristics"`
			ErrorDescription                        string    `json:"ErrorDescription"`
			CreationClassName                       string    `json:"CreationClassName"`
			CurrentClockSpeed                       int       `json:"CurrentClockSpeed"`
			SocketDesignation                       string    `json:"SocketDesignation"`
			NumberOfEnabledCore                     int       `json:"NumberOfEnabledCore"`
			ConfigManagerErrorCode                  int       `json:"ConfigManagerErrorCode"`
			OtherFamilyDescription                  string    `json:"OtherFamilyDescription"`
			ConfigManagerUserConfig                 bool      `json:"ConfigManagerUserConfig"`
			SystemCreationClassName                 string    `json:"SystemCreationClassName"`
			VMMonitorModeExtensions                 bool      `json:"VMMonitorModeExtensions"`
			PowerManagementSupported                bool      `json:"PowerManagementSupported"`
			NumberOfLogicalProcessors               int       `json:"NumberOfLogicalProcessors"`
			VirtualizationFirmwareEnabled           bool      `json:"VirtualizationFirmwareEnabled"`
			SecondLevelAddressTranslationExtensions bool      `json:"SecondLevelAddressTranslationExtensions"`
		} `json:"cpu"`
		Mem [][]struct {
			SKU                  string    `json:"SKU"`
			Tag                  string    `json:"Tag"`
			Name                 string    `json:"Name"`
			Model                string    `json:"Model"`
			Speed                int       `json:"Speed"`
			Status               string    `json:"Status"`
			Caption              string    `json:"Caption"`
			Version              string    `json:"Version"`
			Capacity             int64     `json:"Capacity"`
			BankLabel            string    `json:"BankLabel"`
			DataWidth            int       `json:"DataWidth"`
			PoweredOn            bool      `json:"PoweredOn"`
			Removable            bool      `json:"Removable"`
			Attributes           int       `json:"Attributes"`
			FormFactor           int       `json:"FormFactor"`
			MaxVoltage           int       `json:"MaxVoltage"`
			MemoryType           int       `json:"MemoryType"`
			MinVoltage           int       `json:"MinVoltage"`
			PartNumber           string    `json:"PartNumber"`
			TotalWidth           int       `json:"TotalWidth"`
			TypeDetail           int       `json:"TypeDetail"`
			Description          string    `json:"Description"`
			InstallDate          time.Time `json:"InstallDate"`
			Replaceable          bool      `json:"Replaceable"`
			HotSwappable         bool      `json:"HotSwappable"`
			Manufacturer         string    `json:"Manufacturer"`
			SerialNumber         string    `json:"SerialNumber"`
			DeviceLocator        string    `json:"DeviceLocator"`
			PositionInRow        int       `json:"PositionInRow"`
			SMBIOSMemoryType     int       `json:"SMBIOSMemoryType"`
			ConfiguredVoltage    int       `json:"ConfiguredVoltage"`
			CreationClassName    string    `json:"CreationClassName"`
			InterleavePosition   int       `json:"InterleavePosition"`
			InterleaveDataDepth  int       `json:"InterleaveDataDepth"`
			ConfiguredClockSpeed int       `json:"ConfiguredClockSpeed"`
			OtherIdentifyingInfo string    `json:"OtherIdentifyingInfo"`
		} `json:"mem"`
		Usb [][]struct {
			Name                     string    `json:"Name"`
			Status                   string    `json:"Status"`
			Caption                  string    `json:"Caption"`
			DeviceID                 string    `json:"DeviceID"`
			StatusInfo               int       `json:"StatusInfo"`
			SystemName               string    `json:"SystemName"`
			Description              string    `json:"Description"`
			InstallDate              time.Time `json:"InstallDate"`
			PNPDeviceID              string    `json:"PNPDeviceID"`
			Availability             int       `json:"Availability"`
			ErrorCleared             bool      `json:"ErrorCleared"`
			Manufacturer             string    `json:"Manufacturer"`
			LastErrorCode            int       `json:"LastErrorCode"`
			TimeOfLastReset          time.Time `json:"TimeOfLastReset"`
			ErrorDescription         string    `json:"ErrorDescription"`
			CreationClassName        string    `json:"CreationClassName"`
			ProtocolSupported        int       `json:"ProtocolSupported"`
			MaxNumberControlled      int       `json:"MaxNumberControlled"`
			ConfigManagerErrorCode   int       `json:"ConfigManagerErrorCode"`
			ConfigManagerUserConfig  bool      `json:"ConfigManagerUserConfig"`
			SystemCreationClassName  string    `json:"SystemCreationClassName"`
			PowerManagementSupported bool      `json:"PowerManagementSupported"`
		} `json:"usb"`
		Bios [][]struct {
			Name                           string    `json:"Name"`
			Status                         string    `json:"Status"`
			Caption                        string    `json:"Caption"`
			CodeSet                        string    `json:"CodeSet"`
			Version                        string    `json:"Version"`
			BIOSVersion                    []string  `json:"BIOSVersion"`
			BuildNumber                    string    `json:"BuildNumber"`
			Description                    string    `json:"Description"`
			InstallDate                    time.Time `json:"InstallDate"`
			PrimaryBIOS                    bool      `json:"PrimaryBIOS"`
			ReleaseDate                    time.Time `json:"ReleaseDate"`
			Manufacturer                   string    `json:"Manufacturer"`
			SerialNumber                   string    `json:"SerialNumber"`
			OtherTargetOS                  string    `json:"OtherTargetOS"`
			SMBIOSPresent                  bool      `json:"SMBIOSPresent"`
			CurrentLanguage                string    `json:"CurrentLanguage"`
			LanguageEdition                string    `json:"LanguageEdition"`
			ListOfLanguages                []string  `json:"ListOfLanguages"`
			SMBIOSBIOSVersion              string    `json:"SMBIOSBIOSVersion"`
			SoftwareElementID              string    `json:"SoftwareElementID"`
			IdentificationCode             string    `json:"IdentificationCode"`
			SMBIOSMajorVersion             int       `json:"SMBIOSMajorVersion"`
			SMBIOSMinorVersion             int       `json:"SMBIOSMinorVersion"`
			InstallableLanguages           int       `json:"InstallableLanguages"`
			SoftwareElementState           int       `json:"SoftwareElementState"`
			TargetOperatingSystem          int       `json:"TargetOperatingSystem"`
			SystemBiosMajorVersion         int       `json:"SystemBiosMajorVersion"`
			SystemBiosMinorVersion         int       `json:"SystemBiosMinorVersion"`
			EmbeddedControllerMajorVersion int       `json:"EmbeddedControllerMajorVersion"`
			EmbeddedControllerMinorVersion int       `json:"EmbeddedControllerMinorVersion"`
		} `json:"bios"`
		Disk [][]struct {
			Name                     string    `json:"Name"`
			Size                     int64     `json:"Size"`
			Index                    int       `json:"Index"`
			Model                    string    `json:"Model"`
			Status                   string    `json:"Status"`
			Caption                  string    `json:"Caption"`
			SCSIBus                  int       `json:"SCSIBus"`
			DeviceID                 string    `json:"DeviceID"`
			SCSIPort                 int       `json:"SCSIPort"`
			MediaType                string    `json:"MediaType"`
			Signature                int64     `json:"Signature"`
			Partitions               int       `json:"Partitions"`
			StatusInfo               int       `json:"StatusInfo"`
			SystemName               string    `json:"SystemName"`
			TotalHeads               int       `json:"TotalHeads"`
			Description              string    `json:"Description"`
			InstallDate              time.Time `json:"InstallDate"`
			MediaLoaded              bool      `json:"MediaLoaded"`
			PNPDeviceID              string    `json:"PNPDeviceID"`
			TotalTracks              int       `json:"TotalTracks"`
			Availability             int       `json:"Availability"`
			ErrorCleared             bool      `json:"ErrorCleared"`
			Manufacturer             string    `json:"Manufacturer"`
			MaxBlockSize             int       `json:"MaxBlockSize"`
			MaxMediaSize             int       `json:"MaxMediaSize"`
			MinBlockSize             int       `json:"MinBlockSize"`
			SCSITargetId             int       `json:"SCSITargetId"`
			SerialNumber             string    `json:"SerialNumber"`
			TotalSectors             int       `json:"TotalSectors"`
			InterfaceType            string    `json:"InterfaceType"`
			LastErrorCode            int       `json:"LastErrorCode"`
			NeedsCleaning            bool      `json:"NeedsCleaning"`
			BytesPerSector           int       `json:"BytesPerSector"`
			TotalCylinders           int       `json:"TotalCylinders"`
			SCSILogicalUnit          int       `json:"SCSILogicalUnit"`
			SectorsPerTrack          int       `json:"SectorsPerTrack"`
			DefaultBlockSize         int       `json:"DefaultBlockSize"`
			ErrorDescription         string    `json:"ErrorDescription"`
			ErrorMethodology         string    `json:"ErrorMethodology"`
			FirmwareRevision         string    `json:"FirmwareRevision"`
			CompressionMethod        string    `json:"CompressionMethod"`
			CreationClassName        string    `json:"CreationClassName"`
			TracksPerCylinder        int       `json:"TracksPerCylinder"`
			CapabilityDescriptions   []string  `json:"CapabilityDescriptions"`
			ConfigManagerErrorCode   int       `json:"ConfigManagerErrorCode"`
			NumberOfMediaSupported   int       `json:"NumberOfMediaSupported"`
			ConfigManagerUserConfig  bool      `json:"ConfigManagerUserConfig"`
			SystemCreationClassName  string    `json:"SystemCreationClassName"`
			PowerManagementSupported bool      `json:"PowerManagementSupported"`
		} `json:"disk"`
		CompSys [][]struct {
			Name                      string      `json:"Name"`
			Model                     string      `json:"Model"`
			Roles                     []string    `json:"Roles"`
			Domain                    string      `json:"Domain"`
			Status                    string      `json:"Status"`
			Caption                   string      `json:"Caption"`
			UserName                  string      `json:"UserName"`
			Workgroup                 string      `json:"Workgroup"`
			DomainRole                int         `json:"DomainRole"`
			NameFormat                string      `json:"NameFormat"`
			PowerState                int         `json:"PowerState"`
			ResetCount                int         `json:"ResetCount"`
			ResetLimit                int         `json:"ResetLimit"`
			SystemType                string      `json:"SystemType"`
			WakeUpType                int         `json:"WakeUpType"`
			BootupState               string      `json:"BootupState"`
			DNSHostName               string      `json:"DNSHostName"`
			Description               string      `json:"Description"`
			InstallDate               time.Time   `json:"InstallDate"`
			Manufacturer              string      `json:"Manufacturer"`
			PCSystemType              int         `json:"PCSystemType"`
			PartOfDomain              bool        `json:"PartOfDomain"`
			SystemFamily              string      `json:"SystemFamily"`
			ThermalState              int         `json:"ThermalState"`
			OEMStringArray            []string    `json:"OEMStringArray"`
			PCSystemTypeEx            int         `json:"PCSystemTypeEx"`
			CurrentTimeZone           int         `json:"CurrentTimeZone"`
			PauseAfterReset           int         `json:"PauseAfterReset"`
			ResetCapability           int         `json:"ResetCapability"`
			SystemSKUNumber           string      `json:"SystemSKUNumber"`
			BootROMSupported          bool        `json:"BootROMSupported"`
			ChassisSKUNumber          string      `json:"ChassisSKUNumber"`
			DaylightInEffect          bool        `json:"DaylightInEffect"`
			PowerSupplyState          int         `json:"PowerSupplyState"`
			PrimaryOwnerName          string      `json:"PrimaryOwnerName"`
			BootOptionOnLimit         int         `json:"BootOptionOnLimit"`
			CreationClassName         string      `json:"CreationClassName"`
			HypervisorPresent         bool        `json:"HypervisorPresent"`
			InfraredSupported         bool        `json:"InfraredSupported"`
			ChassisBootupState        int         `json:"ChassisBootupState"`
			NumberOfProcessors        int         `json:"NumberOfProcessors"`
			AdminPasswordStatus       int         `json:"AdminPasswordStatus"`
			PrimaryOwnerContact       string      `json:"PrimaryOwnerContact"`
			TotalPhysicalMemory       int64       `json:"TotalPhysicalMemory"`
			BootOptionOnWatchDog      int         `json:"BootOptionOnWatchDog"`
			FrontPanelResetStatus     int         `json:"FrontPanelResetStatus"`
			PowerOnPasswordStatus     int         `json:"PowerOnPasswordStatus"`
			KeyboardPasswordStatus    int         `json:"KeyboardPasswordStatus"`
			AutomaticManagedPagefile  bool        `json:"AutomaticManagedPagefile"`
			AutomaticResetBootOption  bool        `json:"AutomaticResetBootOption"`
			AutomaticResetCapability  bool        `json:"AutomaticResetCapability"`
			NetworkServerModeEnabled  bool        `json:"NetworkServerModeEnabled"`
			PowerManagementSupported  bool        `json:"PowerManagementSupported"`
			EnableDaylightSavingsTime bool        `json:"EnableDaylightSavingsTime"`
			NumberOfLogicalProcessors int         `json:"NumberOfLogicalProcessors"`
			SupportContactDescription interface{} `json:"SupportContactDescription"`
		} `json:"comp_sys"`
		Graphics [][]struct {
			Name                      string      `json:"Name"`
			Status                    string      `json:"Status"`
			Caption                   string      `json:"Caption"`
			DeviceID                  string      `json:"DeviceID"`
			AdapterRAM                int         `json:"AdapterRAM"`
			DriverDate                time.Time   `json:"DriverDate"`
			SystemName                string      `json:"SystemName"`
			Description               string      `json:"Description"`
			InstallDate               time.Time   `json:"InstallDate"`
			Availability              int         `json:"Availability"`
			DriverVersion             string      `json:"DriverVersion"`
			AdapterDACType            string      `json:"AdapterDACType"`
			MaxRefreshRate            int         `json:"MaxRefreshRate"`
			MinRefreshRate            int         `json:"MinRefreshRate"`
			VideoProcessor            string      `json:"VideoProcessor"`
			TimeOfLastReset           time.Time   `json:"TimeOfLastReset"`
			CurrentRefreshRate        int         `json:"CurrentRefreshRate"`
			MaxMemorySupported        int         `json:"MaxMemorySupported"`
			AdapterCompatibility      string      `json:"AdapterCompatibility"`
			VideoModeDescription      string      `json:"VideoModeDescription"`
			CapabilityDescriptions    interface{} `json:"CapabilityDescriptions"`
			AcceleratorCapabilities   interface{} `json:"AcceleratorCapabilities"`
			InstalledDisplayDrivers   string      `json:"InstalledDisplayDrivers"`
			SystemCreationClassName   string      `json:"SystemCreationClassName"`
			CurrentVerticalResolution int         `json:"CurrentVerticalResolution"`
		} `json:"graphics"`
		BaseBoard [][]struct {
			SKU                     string    `json:"SKU"`
			Tag                     string    `json:"Tag"`
			Name                    string    `json:"Name"`
			Depth                   int       `json:"Depth"`
			Model                   string    `json:"Model"`
			Width                   int       `json:"Width"`
			Height                  int       `json:"Height"`
			Status                  string    `json:"Status"`
			Weight                  int       `json:"Weight"`
			Caption                 string    `json:"Caption"`
			Product                 string    `json:"Product"`
			Version                 string    `json:"Version"`
			PoweredOn               bool      `json:"PoweredOn"`
			Removable               bool      `json:"Removable"`
			PartNumber              string    `json:"PartNumber"`
			SlotLayout              string    `json:"SlotLayout"`
			Description             string    `json:"Description"`
			InstallDate             time.Time `json:"InstallDate"`
			Replaceable             bool      `json:"Replaceable"`
			HostingBoard            bool      `json:"HostingBoard"`
			HotSwappable            bool      `json:"HotSwappable"`
			Manufacturer            string    `json:"Manufacturer"`
			SerialNumber            string    `json:"SerialNumber"`
			ConfigOptions           []string  `json:"ConfigOptions"`
			CreationClassName       string    `json:"CreationClassName"`
			SpecialRequirements     bool      `json:"SpecialRequirements"`
			OtherIdentifyingInfo    string    `json:"OtherIdentifyingInfo"`
			RequiresDaughterBoard   bool      `json:"RequiresDaughterBoard"`
			RequirementsDescription string    `json:"RequirementsDescription"`
		} `json:"base_board"`
		CompSysProd [][]struct {
			Name              string `json:"Name"`
			UUID              string `json:"UUID"`
			Vendor            string `json:"Vendor"`
			Caption           string `json:"Caption"`
			Version           string `json:"Version"`
			SKUNumber         string `json:"SKUNumber"`
			Description       string `json:"Description"`
			IdentifyingNumber string `json:"IdentifyingNumber"`
		} `json:"comp_sys_prod"`
		NetworkConfig [][]struct {
			MTU                          int           `json:"MTU"`
			Index                        int           `json:"Index"`
			Caption                      string        `json:"Caption"`
			IPSubnet                     []string      `json:"IPSubnet"`
			DNSDomain                    string        `json:"DNSDomain"`
			IGMPLevel                    int           `json:"IGMPLevel"`
			IPAddress                    []string      `json:"IPAddress"`
			IPEnabled                    bool          `json:"IPEnabled"`
			SettingID                    string        `json:"SettingID"`
			DHCPServer                   string        `json:"DHCPServer"`
			DefaultTOS                   int           `json:"DefaultTOS"`
			DefaultTTL                   int           `json:"DefaultTTL"`
			MACAddress                   string        `json:"MACAddress"`
			DHCPEnabled                  bool          `json:"DHCPEnabled"`
			DNSHostName                  string        `json:"DNSHostName"`
			Description                  string        `json:"Description"`
			ServiceName                  string        `json:"ServiceName"`
			WINSScopeID                  string        `json:"WINSScopeID"`
			DatabasePath                 string        `json:"DatabasePath"`
			KeepAliveTime                int           `json:"KeepAliveTime"`
			TcpWindowSize                int           `json:"TcpWindowSize"`
			InterfaceIndex               int           `json:"InterfaceIndex"`
			ArpUseEtherSNAP              bool          `json:"ArpUseEtherSNAP"`
			DHCPLeaseExpires             time.Time     `json:"DHCPLeaseExpires"`
			DefaultIPGateway             []string      `json:"DefaultIPGateway"`
			DHCPLeaseObtained            time.Time     `json:"DHCPLeaseObtained"`
			KeepAliveInterval            int           `json:"KeepAliveInterval"`
			NumForwardPackets            int           `json:"NumForwardPackets"`
			TcpNumConnections            int           `json:"TcpNumConnections"`
			WINSPrimaryServer            string        `json:"WINSPrimaryServer"`
			IPConnectionMetric           int           `json:"IPConnectionMetric"`
			IPUseZeroBroadcast           bool          `json:"IPUseZeroBroadcast"`
			WINSHostLookupFile           string        `json:"WINSHostLookupFile"`
			DeadGWDetectEnabled          bool          `json:"DeadGWDetectEnabled"`
			ForwardBufferMemory          int           `json:"ForwardBufferMemory"`
			IPSecPermitTCPPorts          []interface{} `json:"IPSecPermitTCPPorts"`
			IPSecPermitUDPPorts          []interface{} `json:"IPSecPermitUDPPorts"`
			PMTUBHDetectEnabled          bool          `json:"PMTUBHDetectEnabled"`
			TcpipNetbiosOptions          int           `json:"TcpipNetbiosOptions"`
			WINSSecondaryServer          string        `json:"WINSSecondaryServer"`
			ArpAlwaysSourceRoute         bool          `json:"ArpAlwaysSourceRoute"`
			DNSServerSearchOrder         []string      `json:"DNSServerSearchOrder"`
			PMTUDiscoveryEnabled         bool          `json:"PMTUDiscoveryEnabled"`
			IPSecPermitIPProtocols       []interface{} `json:"IPSecPermitIPProtocols"`
			IPFilterSecurityEnabled      bool          `json:"IPFilterSecurityEnabled"`
			WINSEnableLMHostsLookup      bool          `json:"WINSEnableLMHostsLookup"`
			TcpMaxDataRetransmissions    int           `json:"TcpMaxDataRetransmissions"`
			DNSDomainSuffixSearchOrder   []string      `json:"DNSDomainSuffixSearchOrder"`
			FullDNSRegistrationEnabled   bool          `json:"FullDNSRegistrationEnabled"`
			TcpUseRFC1122UrgentPointer   bool          `json:"TcpUseRFC1122UrgentPointer"`
			DNSEnabledForWINSResolution  bool          `json:"DNSEnabledForWINSResolution"`
			DomainDNSRegistrationEnabled bool          `json:"DomainDNSRegistrationEnabled"`
			TcpMaxConnectRetransmissions int           `json:"TcpMaxConnectRetransmissions"`
		} `json:"network_config"`
		DesktopMonitor [][]struct {
			Name                     string    `json:"Name"`
			Status                   string    `json:"Status"`
			Caption                  string    `json:"Caption"`
			DeviceID                 string    `json:"DeviceID"`
			IsLocked                 bool      `json:"IsLocked"`
			Bandwidth                int       `json:"Bandwidth"`
			StatusInfo               int       `json:"StatusInfo"`
			SystemName               string    `json:"SystemName"`
			Description              string    `json:"Description"`
			DisplayType              int       `json:"DisplayType"`
			InstallDate              time.Time `json:"InstallDate"`
			MonitorType              string    `json:"MonitorType"`
			PNPDeviceID              string    `json:"PNPDeviceID"`
			ScreenWidth              int       `json:"ScreenWidth"`
			Availability             int       `json:"Availability"`
			ErrorCleared             bool      `json:"ErrorCleared"`
			ScreenHeight             int       `json:"ScreenHeight"`
			LastErrorCode            int       `json:"LastErrorCode"`
			ErrorDescription         string    `json:"ErrorDescription"`
			CreationClassName        string    `json:"CreationClassName"`
			MonitorManufacturer      string    `json:"MonitorManufacturer"`
			PixelsPerXLogicalInch    int       `json:"PixelsPerXLogicalInch"`
			PixelsPerYLogicalInch    int       `json:"PixelsPerYLogicalInch"`
			ConfigManagerErrorCode   int       `json:"ConfigManagerErrorCode"`
			ConfigManagerUserConfig  bool      `json:"ConfigManagerUserConfig"`
			SystemCreationClassName  string    `json:"SystemCreationClassName"`
			PowerManagementSupported bool      `json:"PowerManagementSupported"`
		} `json:"desktop_monitor"`
		NetworkAdapter [][]struct {
			GUID                     string      `json:"GUID"`
			Name                     string      `json:"Name"`
			Index                    int         `json:"Index"`
			Speed                    float64     `json:"Speed"`
			Status                   string      `json:"Status"`
			Caption                  string      `json:"Caption"`
			DeviceID                 string      `json:"DeviceID"`
			MaxSpeed                 int         `json:"MaxSpeed"`
			AutoSense                bool        `json:"AutoSense"`
			MACAddress               string      `json:"MACAddress"`
			NetEnabled               bool        `json:"NetEnabled"`
			StatusInfo               int         `json:"StatusInfo"`
			SystemName               string      `json:"SystemName"`
			AdapterType              string      `json:"AdapterType"`
			Description              string      `json:"Description"`
			InstallDate              time.Time   `json:"InstallDate"`
			PNPDeviceID              string      `json:"PNPDeviceID"`
			ProductName              string      `json:"ProductName"`
			ServiceName              string      `json:"ServiceName"`
			Availability             int         `json:"Availability"`
			ErrorCleared             bool        `json:"ErrorCleared"`
			Manufacturer             string      `json:"Manufacturer"`
			AdapterTypeID            int         `json:"AdapterTypeID"`
			LastErrorCode            int         `json:"LastErrorCode"`
			InterfaceIndex           int         `json:"InterfaceIndex"`
			NetConnectionID          string      `json:"NetConnectionID"`
			PhysicalAdapter          bool        `json:"PhysicalAdapter"`
			TimeOfLastReset          time.Time   `json:"TimeOfLastReset"`
			ErrorDescription         string      `json:"ErrorDescription"`
			NetworkAddresses         interface{} `json:"NetworkAddresses"`
			PermanentAddress         string      `json:"PermanentAddress"`
			CreationClassName        string      `json:"CreationClassName"`
			MaxNumberControlled      int         `json:"MaxNumberControlled"`
			NetConnectionStatus      int         `json:"NetConnectionStatus"`
			ConfigManagerErrorCode   int         `json:"ConfigManagerErrorCode"`
			ConfigManagerUserConfig  bool        `json:"ConfigManagerUserConfig"`
			SystemCreationClassName  string      `json:"SystemCreationClassName"`
			PowerManagementSupported bool        `json:"PowerManagementSupported"`
		} `json:"network_adapter"`
	} `json:"wmi_detail"`
	TimeZone               interface{} `json:"time_zone"`
	MaintenanceMode        bool        `json:"maintenance_mode"`
	BlockPolicyInheritance bool        `json:"block_policy_inheritance"`
	PendingActionsCount    int         `json:"pending_actions_count"`
	HasPatchesPending      bool        `json:"has_patches_pending"`
	Site                   int         `json:"site"`
	Policy                 interface{} `json:"policy"`
}

type AppliedPolicies struct {
	AgentPolicy   interface{}   `json:"agent_policy"`
	SitePolicy    interface{}   `json:"site_policy"`
	ClientPolicy  interface{}   `json:"client_policy"`
	DefaultPolicy DefaultPolicy `json:"default_policy"`
}

type DefaultPolicy struct {
	Id              int           `json:"id"`
	CreatedBy       string        `json:"created_by"`
	CreatedTime     string        `json:"created_time"`
	ModifiedBy      string        `json:"modified_by"`
	ModifiedTime    string        `json:"modified_time"`
	Name            string        `json:"name"`
	Desc            string        `json:"desc"`
	Active          bool          `json:"active"`
	Enforced        bool          `json:"enforced"`
	AlertTemplate   interface{}   `json:"alert_template"`
	ExcludedSites   []interface{} `json:"excluded_sites"`
	ExcludedClients []interface{} `json:"excluded_clients"`
	ExcludedAgents  []interface{} `json:"excluded_agents"`
}

type EffectivePatchPolicy struct {
	Id                     int         `json:"id"`
	CreatedBy              interface{} `json:"created_by"`
	CreatedTime            string      `json:"created_time"`
	ModifiedBy             interface{} `json:"modified_by"`
	ModifiedTime           string      `json:"modified_time"`
	Critical               string      `json:"critical"`
	Important              string      `json:"important"`
	Moderate               string      `json:"moderate"`
	Low                    string      `json:"low"`
	Other                  string      `json:"other"`
	RunTimeHour            int         `json:"run_time_hour"`
	RunTimeFrequency       string      `json:"run_time_frequency"`
	RunTimeDays            []int       `json:"run_time_days"`
	RunTimeDay             int         `json:"run_time_day"`
	RebootAfterInstall     string      `json:"reboot_after_install"`
	ReprocessFailedInherit bool        `json:"reprocess_failed_inherit"`
	ReprocessFailed        bool        `json:"reprocess_failed"`
	ReprocessFailedTimes   int         `json:"reprocess_failed_times"`
	EmailIfFail            bool        `json:"email_if_fail"`
	Agent                  int         `json:"agent"`
	Policy                 interface{} `json:"policy"`
}

type Disks struct {
	Device string `json:"device"`
	Fstype string `json:"fstype"`
	//Total   string `json:"total"`
	Total uint64 `json:"total"`
	//Used    string `json:"used"`
	Used uint64 `json:"used"`
	//Free    string `json:"free"`
	Free uint64 `json:"free"`
	//Percent int    `json:"percent"`
	Percent float64 `json:"percent"`
}

type AgentsIdWinupdatepolicy struct {
	Id                     int         `json:"id"`
	CreatedBy              interface{} `json:"created_by"`
	CreatedTime            string      `json:"created_time"`
	ModifiedBy             interface{} `json:"modified_by"`
	ModifiedTime           string      `json:"modified_time"`
	Critical               string      `json:"critical"`
	Important              string      `json:"important"`
	Moderate               string      `json:"moderate"`
	Low                    string      `json:"low"`
	Other                  string      `json:"other"`
	RunTimeHour            int         `json:"run_time_hour"`
	RunTimeFrequency       string      `json:"run_time_frequency"`
	RunTimeDays            []int       `json:"run_time_days"`
	RunTimeDay             int         `json:"run_time_day"`
	RebootAfterInstall     string      `json:"reboot_after_install"`
	ReprocessFailedInherit bool        `json:"reprocess_failed_inherit"`
	ReprocessFailed        bool        `json:"reprocess_failed"`
	ReprocessFailedTimes   int         `json:"reprocess_failed_times"`
	EmailIfFail            bool        `json:"email_if_fail"`
	Agent                  int         `json:"agent"`
	Policy                 interface{} `json:"policy"`
}

type AgentsIdChecks struct {
	Total            int  `json:"total"`
	Passing          int  `json:"passing"`
	Failing          int  `json:"failing"`
	Warning          int  `json:"warning"`
	Info             int  `json:"info"`
	HasFailingChecks bool `json:"has_failing_checks"`
}
