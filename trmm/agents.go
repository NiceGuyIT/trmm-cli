// Package trmm integrates with the Tactical RMM API.
package trmm

import "fmt"

// GetAgentsAgents will get the Agents from the TRMM API.
func (t *TRMM) GetAgentsAgents() *TRMM {
	if t.err != nil {
		return t
	}

	t.Agents.Agents = t.setURL("agents").
		apiGet([]AgentsAgents{}).
		response.Result().(*[]AgentsAgents)
	return t
}

// GetAgentsHistory will get the AgentsHistory from the TRMM API.
func (t *TRMM) GetAgentsHistory() *TRMM {
	if t.err != nil {
		return t
	}

	t.Agents.History = t.setURL("agents/history").
		apiGet([]AgentsHistory{}).
		response.Result().(*[]AgentsHistory)
	return t
}

// GetAgentsNotes will get the AgentsNotes from the TRMM API.
func (t *TRMM) GetAgentsNotes() *TRMM {
	if t.err != nil {
		return t
	}

	t.Agents.Notes = t.setURL("agents/notes").
		apiGet([]AgentsNotes{}).
		response.Result().(*[]AgentsNotes)
	return t
}

// GetAgentsVersions will get the AgentsVersions from the TRMM API.
func (t *TRMM) GetAgentsVersions() *TRMM {
	if t.err != nil {
		return t
	}

	t.Agents.Versions = t.setURL("agents/versions").
		apiGet(AgentsVersions{}).
		response.Result().(*AgentsVersions)
	return t
}

func (t *TRMM) GetAgentsClient(client string) *TRMM {
	if t.err != nil {
		return t
	}

	t.GetClient(client)
	
	t.Agents.Agents = t.setURL(fmt.Sprintf("agents?client=%d", t.Client.Id)).
		apiGet([]AgentsAgents{}).
		response.Result().(*[]AgentsAgents)

	return t
}

func (t *TRMM) DeleteAgent(agentId string) *TRMM {
	if t.err != nil {
		return t
	}

	deleteUrl := fmt.Sprintf("agents/%s/", agentId)
	t.setURL(deleteUrl).apiDelete()
	return t
}
