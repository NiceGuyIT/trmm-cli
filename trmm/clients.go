// Package trmm integrates with the Tactical RMM API.
package trmm

import (
	"errors"
	"fmt"
)

// GetClients will get the Clients from the TRMM API.
func (t *TRMM) GetClients() *TRMM {
	if t.err != nil {
		return t
	}

	t.Clients = t.setURL("clients").
		apiGet([]Client{}).
		response.Result().(*[]Client)
	return t
}

func (t *TRMM) GetClient(client string) *TRMM {
	t.Clients = t.setURL("clients").apiGet([]Client{}).
		response.Result().(*[]Client)

	var foundClient Client
	for _, c := range *t.Clients {
		if(c.Name == client) {
			foundClient = c
		}
	}

	t.Client = &foundClient
	return t
}

func (t *TRMM) CreateClient(client string, site string) *TRMM {
	newClient := CreateClientWrapper {
		Client: CreateClient {
			Name: client,
		},
		Site: CreateSite {
			Name: site,
		},
	}
	
	t.setURL("clients/").apiPost(newClient)
	return t
}

func (t *TRMM) RenameClient(client string, newName string) *TRMM {
	t.GetClient(client)
	t.Client.Name = newName
	wrapper := ClientWrapper {
		Client: *t.Client,
	}

	url := fmt.Sprintf("clients/%d/", t.Client.Id)
	t.setURL(url).apiPut(wrapper)
	return t
}

func (t *TRMM) DeleteClient(client string) *TRMM {
	t.GetClient(client)
	if t.Client == nil {
		t.err = errors.New("Could not get client")	
		return t
	}

	url := fmt.Sprintf("clients/%d/", t.Client.Id)
	t.setURL(url).apiDelete()
	return t
}

func (t *TRMM) Offboard(client string, force bool) (bool, error) {
	if t.err != nil {
		return false, t.err
	}

	t.GetClient(client)
	if t.Client.Name != client {
		t.err = errors.New("Could not get client")

		return false, t.err
	}

	t.GetAgentsClient(t.Client.Name)
	for _, agent := range *t.Agents.Agents {
		//remove online agents here
		if agent.Status == "online" {
			t.DeleteAgent(agent.AgentId)
		} else {
			if force {
				t.DeleteAgent(agent.AgentId)
			}
		}
	}

	//make sure all agents are gone
	t.GetAgentsClient(t.Client.Name)
	if len(*t.Agents.Agents) != 0 {
		return false, errors.New("Could not offboard agents, use force to offboard offline agents")
	}

	//remove client
	t.DeleteClient(t.Client.Name)

	return true, nil
}
