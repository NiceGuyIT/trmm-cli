module gitlab.com/NiceGuyIT/trmm-cli

go 1.18

require (
	github.com/alecthomas/kong v0.6.1
	github.com/go-resty/resty/v2 v2.7.0
	github.com/gocarina/gocsv v0.0.0-20220707092902-b9da1f06c77e
	github.com/rs/zerolog v1.27.0
)

require (
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/net v0.0.0-20220708220712-1185a9018129 // indirect
	golang.org/x/sys v0.0.0-20220708085239-5a0f0661e09d // indirect
)
