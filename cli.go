// SPDX-License-Identifier: PostgreSQL

// Package trmmcli is the higher level interface to Tactical RMM.
package trmmcli

import (
	"github.com/alecthomas/kong"
)

// CLI contains the command line configuration
type CLI struct {

	// Accounts command is to work with TRMM accounts.
	Accounts struct {
		Users   struct{} `kong:"cmd='',group='accounts',help='Work with the accounts users endpoint.'"`
		Roles   struct{} `kong:"cmd='',group='accounts',help='Work with the accounts roles endpoint.'"`
		APIKeys struct{} `kong:"cmd='',group='accounts',help='Work with the accounts api-keys endpoint.'"`
	} `kong:"cmd='',group='accounts',help='Work with the accounts endpoint.'"`

	// Agents command is to work with TRMM agents.
	Agents struct {
		Agents   struct{} `kong:"cmd='',group='agents',help='Work with the agents agents endpoint.'"`
		History  struct{} `kong:"cmd='',group='agents',help='Work with the agents history endpoint.'"`
		Notes    struct{} `kong:"cmd='',group='agents',help='Work with the agents notes endpoint.'"`
		Versions struct{} `kong:"cmd='',group='agents',help='Work with the agents versions endpoint.'"`
	} `kong:"cmd='',group='agents',help='Work with the agents endpoint.'"`

	// Alerts command is to work with TRMM alerts.
	Alerts struct {
		Templates struct{} `kong:"cmd='',group='alerts',help='Work with the alerts templates endpoint.'"`
	} `kong:"cmd='',group='alerts',help='Work with the alerts endpoint.'"`

	// Checks command is to work with TRMM checks.
	Checks struct {
	} `kong:"cmd='',group='checks',help='Work with the checks endpoint.'"`

	// Clients command is to work with TRMM clients.
	Clients struct {
		Clients struct{} `kong:"cmd='',group='clients',help='Work with the clients endpoint.'"`
		Sites   struct{} `kong:"cmd='',group='clients',help='Work with the clients sites endpoint.'"`
		Offboard struct{
			ClientParam string `arg:"" name:"client" help:"Specify the client."`
			Force bool `help:"Force offboard the client."`
		} `kong:"cmd='',group='clients',help='Offboard a client.'"`
	} `kong:"cmd='',group='clients',help='Work with the clients endpoint.'"`

	// Core command is to work with TRMM core.
	Core struct {
		CodeSign     struct{} `kong:"cmd='',group='core',help='Work with the core code-sign endpoint.'"`
		CustomFields struct{} `kong:"cmd='',group='core',help='Work with the core custom-fields endpoint.'"`
		DashInfo     struct{} `kong:"cmd='',group='core',help='Work with the core dash-info endpoint.'"`
		KeyStore     struct{} `kong:"cmd='',group='core',help='Work with the core key-store endpoint.'"`
		Settings     struct{} `kong:"cmd='',group='core',help='Work with the core settings endpoint.'"`
		Version      struct{} `kong:"cmd='',group='core',help='Work with the core version endpoint.'"`
	} `kong:"cmd='',group='core',help='Work with the core endpoint.'"`

	// Logs command is to work with TRMM logs.
	Logs struct {
		PendingActions struct{} `kong:"cmd='',group='core',help='Work with the logs pending-actions endpoint.'"`
	} `kong:"cmd='',group='logs',help='Work with the logs endpoint.'"`

	// Scripts command is to work with TRMM scripts.
	Scripts struct {
		Scripts  struct{} `kong:"cmd='',group='scripts',help='Work with the scripts endpoint.'"`
		Snippets struct{} `kong:"cmd='',group='scripts',help='Work with the scripts snippets endpoint.'"`
	} `kong:"cmd='',group='scripts',help='Work with the scripts endpoint.'"`

	// Services command is to work with TRMM services.
	Services struct {
	} `kong:"cmd='',group='services',help='Work with the services endpoint.'"`

	// Software command for working with TRMM software.
	Software struct {
		Software struct{} `kong:"cmd='',group='software',help='Work with the software endpoint.'"`
		Chocos   struct{} `kong:"cmd='',group='software',help='Work with the software chocos endpoint.'"`
	} `kong:"cmd='',group='software',help='Work with the software endpoint.'"`

	// Tasks command is to work with TRMM tasks.
	Tasks struct {
	} `kong:"cmd='',group='tasks',help='Work with the tasks endpoint.'"`

	// Winupdate command is to work with TRMM winupdate.
	Winupdate struct {
	} `kong:"cmd='',group='winupdate',help='Work with the winupdate endpoint.'"`

	/**
	 * Non-Tactical commands
	 */
	// Logging options
	Logging struct {
		// https://github.com/rs/zerolog#leveled-logging
		Level string `kong:"enum='debug,info,warn,error',default='warn',help='Log level'"`
		Type  string `kong:"enum='json,console',default='console',help='Log type'"`
	} `kong:"embed='',prefix='log-'"`

	// Version
	Version struct {
	} `kong:"cmd='',group='version',name='version'"`

	// License
	License struct {
	} `kong:"cmd='',group='license',name='license'"`
}

// kongContext is Kong's context
var kongContext *kong.Context

// NewCLI starts a new configuration.
func NewCLI() *CLI {
	return &CLI{}
}

// ParseCLI command line options
func (cli *CLI) ParseCLI() *CLI {
	kongContext = kong.Parse(cli,
		kong.Name("trmm-cli"),
		kong.Description("trmm-cli is a command line interface to the TRMM API."),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Summary: false,
			//Compact:             true,
			//NoExpandSubcommands: true,
		}))

	SetLogging(&LoggingConfig{
		Level: cli.Logging.Level,
		Type:  cli.Logging.Type,
	})

	return nil
}
