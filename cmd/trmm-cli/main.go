// SPDX-License-Identifier: PostgreSQL

// Package main is for the trmm-cli binary.
package main

import (
	trmmcli "gitlab.com/NiceGuyIT/trmm-cli"
	"os"
)

func main() {
	_ = trmmcli.
		NewTacticalCLI().
		SetDomain(os.Getenv("TRMM_API_DOMAIN")).
		SetAPIKey(os.Getenv("TRMM_API_KEY")).
		Run()
}
