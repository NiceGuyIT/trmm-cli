# trmm-cli

TRMM cli using the API. The API schema can be found at `https://api.examlpe.com/api/schema/swagger-ui/#/`.

## Installation

`trmm-cli` requires Go 1.18. Using the Go toolchain, this command will download, compile and install `trmm-cli` in `$HOME/go/bin` (*nix) or `%USERPROFILE%\go` (Windows), depending on your [`$GOPATH`](https://pkg.go.dev/cmd/go#hdr-GOPATH_environment_variable).

```bash
go install gitlab.com/NiceGuyIT/trmm-cli/cmd/trmm-cli@latest
```

## Usage

Most "listing" endpoints work. Listing endpoints output a flat list of information suitable for import into a spreadsheet. Some endpoints like `/software`, have a few top level fields and then a large list of items in other fields. This hierarchy is not suitable for CSV files.

Currently, `trmm-cli` outputs in CSV format which looses some detail when compared to a hierarchical format like JSON.

First, export your domain and API key.

```bash
cp .env-example .env
# Edit .env and add your domain and API key
export $(grep -vE "^(#.*|\s*)$" ../../.env)

# OR
export TRMM_API_DOMAIN="api.example.com"
export TRMM_API_KEY="ENTER_API_KEY_HERE"
```

Then run the command for the endpoint

```bash
trmm-cli checks > checks.csv
trmm-cli clients clients > clients-clients.csv
trmm-cli agents > agents.csv
```

And of course there is the help command.

```bash
Usage: trmm-cli <command>

trmm-cli is a command line interface to the TRMM API.

Flags:
  -h, --help                  Show context-sensitive help.
      --log-level="warn"      Log level
      --log-type="console"    Log type

accounts
  accounts users
    Work with the accounts users endpoint.

  accounts roles
    Work with the accounts roles endpoint.

  accounts api-keys
    Work with the accounts api-keys endpoint.

agents
  agents agents
    Work with the agents agents endpoint.

  agents history
    Work with the agents history endpoint.

  agents notes
    Work with the agents notes endpoint.

  agents versions
    Work with the agents versions endpoint.

alerts
  alerts templates
    Work with the alerts templates endpoint.

checks
  checks
    Work with the checks endpoint.

clients
  clients clients
    Work with the clients endpoint.

  clients sites
    Work with the clients sites endpoint.

core
  core code-sign
    Work with the core code-sign endpoint.

  core custom-fields
    Work with the core custom-fields endpoint.

  core dash-info
    Work with the core dash-info endpoint.

  core key-store
    Work with the core key-store endpoint.

  core settings
    Work with the core settings endpoint.

  core version
    Work with the core version endpoint.

  logs pending-actions
    Work with the logs pending-actions endpoint.

scripts
  scripts scripts
    Work with the scripts endpoint.

  scripts snippets
    Work with the scripts snippets endpoint.

services
  services
    Work with the services endpoint.

software
  software software
    Work with the software endpoint.

  software chocos
    Work with the software chocos endpoint.

tasks
  tasks
    Work with the tasks endpoint.

winupdate
  winupdate
    Work with the winupdate endpoint.

version
  version

license
  license

Run "trmm-cli <command> --help" for more information on a command.

trmm-cli: error: unexpected argument help
```

### Notes

The `/api/v3` APIs do not work as they need a different authentication method.

## Development

This project follows the typical Go setup.

```bash
git clone https://gitlab.com/NiceGuyIT/trmm-cli
cd trmm-cli
```

The `.env` file is not used by `trmm-cli` directly. Instead, the export command will export the domain and API key to be used when running the program.

```bash
cp .env-example .env

## Testing
Running all tests.
Run in the root directory.

```
env TRMM_API_DOMAIN=api.domain.com TRMM_API_KEY=asdf123 go test -v ./...
```

Running single tests.
```
env TRMM_API_DOMAIN=api.domain.com TRMM_API_KEY=asdf123 go test -v -run TestGetAgentsClient
```

# Edit .env and add your domain and API key
export $(grep -vE "^(#.*|\s*)$" ../../.env)
```


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/NiceGuyIT/trmm-cli.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/NiceGuyIT/trmm-cli/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
